export type TelephoneNumber = {
  countryCode: string;
  mainNumber: string;
};
