import { mount } from '@vue/test-utils'

import TelephoneNumber from '@/components/TelephoneNumber.vue'
import type { TelephoneNumber as TelephoneNumberEntity } from '@/entities/TelephoneNumber'

describe('HelloWorld.vue', () => {
  it('renders correctly in its default state', () => {
    const wrapper = mount(TelephoneNumber)

    expect(wrapper.isVisible()).toBe(true)

    const countryCodeWrapper = wrapper.get('[data-test-telephone-number="country-code"]')

    expect(countryCodeWrapper.isVisible()).toBe(true)
    expect((countryCodeWrapper.element as HTMLInputElement).value).toBe('')

    const mainNumberWrapper = wrapper.get('[data-test-telephone-number="main-number"]')

    expect(mainNumberWrapper.isVisible()).toBe(true)
    expect((mainNumberWrapper.element as HTMLInputElement).value).toBe('')
  })

  it('uses the initial value passed as a prop', () => {
    const entity: TelephoneNumberEntity = {
      countryCode: '+1234',
      mainNumber: '123456789'
    }

    const wrapper = mount(TelephoneNumber, {
      props: {
        modelValue: entity
      }
    })

    const countryCodeWrapper = wrapper.get('[data-test-telephone-number="country-code"]')

    expect((countryCodeWrapper.element as HTMLInputElement).value).toBe(entity.countryCode)

    const mainNumberWrapper = wrapper.get('[data-test-telephone-number="main-number"]')

    expect((mainNumberWrapper.element as HTMLInputElement).value).toBe(entity.mainNumber)
  })

  it('can be used with v-model', async () => {
    const wrapper = mount(TelephoneNumber)

    const countryCodeWrapper = wrapper.get('[data-test-telephone-number="country-code"]')

    await countryCodeWrapper.setValue('+1234')

    expect((wrapper.emitted('update:modelValue')?.[0] as any)[0]).toStrictEqual({
      countryCode: '+1234',
      mainNumber: ''
    })

    const mainNumberWrapper = wrapper.get('[data-test-telephone-number="main-number"]')

    await mainNumberWrapper.setValue('123456789')

    expect((wrapper.emitted('update:modelValue')?.[1] as any)[0]).toStrictEqual({
      countryCode: '+1234',
      mainNumber: '123456789'
    })
  })

  it('removes invalid characters while preserving valid ones', async () => {
    const wrapper = mount(TelephoneNumber)

    const countryCodeWrapper = wrapper.get('[data-test-telephone-number="country-code"]')

    await countryCodeWrapper.setValue('+1foo')

    expect((wrapper.emitted('update:modelValue')?.[0] as any)[0]).toStrictEqual({
      countryCode: '+1',
      mainNumber: ''
    })

    const mainNumberWrapper = wrapper.get('[data-test-telephone-number="main-number"]')

    await mainNumberWrapper.setValue('123bar')

    expect((wrapper.emitted('update:modelValue')?.[1] as any)[0]).toStrictEqual({
      countryCode: '+1',
      mainNumber: '123'
    })
  })
})
